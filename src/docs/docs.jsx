import React from 'react';
import ReactMarkdown from 'react-markdown';

import './doc.scss';

const docs = {
    overview: require('@siplay/interview-docs/src/OVERVIEW.md'),
    'get-started': require('@siplay/interview-docs/src/GETSTARTED.md'),
    'exercise': require('@siplay/interview-docs/src/EXERCISE.md'),
    'dev-api': require('@siplay/simple-rest-api/API.md')
};

export default class Docs extends React.Component {

    render() {
        let doc = docs[this.props.match.params.docId];
        return (
            <docs>
                <ReactMarkdown source={doc} />
            </docs>
        )
    }

}