import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import ApiDemo  from './api-demo/api.demo';
import Docs     from './docs/docs';
import Home     from './home/home';
import Tasks    from './tasks/tasks';

import './app.scss';

export default class AppComponent extends React.Component {

    render() {
        return (
            <Router>
                <div className="row app-body">
                    <div className="menu col-2">
                        <header>
                            <Link className="logo" to="/" />
                        </header>

                        <div>
                            <Link to="/">Home</Link>
                        </div>
                        <div>
                            <Link to="/docs/overview">Overview</Link>
                        </div>
                        <div>
                            <Link to="/docs/get-started">Get Started</Link>
                        </div>
                        <div>
                            <Link to="/docs/exercise">Exercise</Link>
                        </div>
                        <div>
                            <Link to="/docs/dev-api">Dev API</Link>
                        </div>
                        <div>
                            <Link to="/api-demo">Dev API Demo</Link>
                        </div>
                        <div>
                            <Link to="/tasks">Tasks</Link>
                        </div>
                    </div>

                    <div className="app-content col-10">
                        <Route exact path="/" component={Home} />
                        <Route path="/docs/:docId" component={Docs} />
                        <Route path="/api-demo" component={ApiDemo} />
                        <Route exact path="/tasks" component={Tasks} />
                    </div>
                </div>
            </Router>
        );
    }

}