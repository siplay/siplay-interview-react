import React from 'react';
import ReactMarkdown from 'react-markdown';

import './instructions.scss';

export default class InstructionsComponent extends React.Component {

    render() {
        return (
            <instructions>
                <ReactMarkdown source={require('../README.md')} />
            </instructions>
        )
    }

}