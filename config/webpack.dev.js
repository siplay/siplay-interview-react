const RestApi = require('@siplay/simple-rest-api').RestApi;

module.exports = {

    devtool: 'cheap-module-source-map',

    devServer: {
        host: '0.0.0.0',
        port: '3000',
        disableHostCheck: true,
        setup: app => new RestApi(app),
        open: true
    }

};